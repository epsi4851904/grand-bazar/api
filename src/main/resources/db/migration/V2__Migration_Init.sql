CREATE TABLE article
(
    id            INT         NOT NULL,
    name          VARCHAR(50) NOT NULL,
    `description` VARCHAR(50) NOT NULL,
    price DOUBLE NULL,
    created_at    datetime NULL,
    category      VARCHAR(255) NULL,
    status        VARCHAR(255) NULL,
    user_id       INT         NOT NULL,
    CONSTRAINT pk_article PRIMARY KEY (id)
);

CREATE TABLE note
(
    id      INT NOT NULL,
    name    VARCHAR(255) NULL,
    avis    VARCHAR(255) NULL,
    note    INT NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT pk_note PRIMARY KEY (id)
);

CREATE TABLE user
(
    id        INT          NOT NULL,
    firstname VARCHAR(50)  NOT NULL,
    lastname  VARCHAR(50)  NOT NULL,
    email     VARCHAR(50)  NOT NULL,
    password  VARCHAR(255) NOT NULL,
    `role`    VARCHAR(255) NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
);

ALTER TABLE user
    ADD CONSTRAINT uc_user_email UNIQUE (email);

ALTER TABLE article
    ADD CONSTRAINT FK_ARTICLE_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE note
    ADD CONSTRAINT FK_NOTE_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

DROP TABLE employees;