package com.example.api.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoteDTO {

    private String name;

    private String avis;

    private Integer note;
}
