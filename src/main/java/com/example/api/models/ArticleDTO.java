package com.example.api.models;

import com.example.api.enums.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArticleDTO {
    private String name;
    private String description;
    private Double price;
    private Category category;
}
