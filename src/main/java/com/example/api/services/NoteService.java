package com.example.api.services;

import com.example.api.entities.Note;
import com.example.api.entities.User;
import com.example.api.models.NoteDTO;
import com.example.api.repositories.NoteRepository;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepository;

    public void create(NoteDTO noteDTO, User user){
        Note note = new Note();
        note.setName(noteDTO.getName());
        note.setAvis(noteDTO.getAvis());
        note.setNote(noteDTO.getNote());
        note.setUser(user);
        noteRepository.save(note);
    }
}
