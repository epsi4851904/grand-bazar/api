package com.example.api.services;

import com.example.api.entities.Article;
import com.example.api.entities.User;
import com.example.api.enums.Category;
import com.example.api.enums.StatusArticle;
import com.example.api.models.ArticleDTO;
import com.example.api.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public List<Article> getArticlesByFilter(Category category, Double minPrice, Double maxPrice, String name) {
        List<Article> articles = articleRepository.findByStatus(StatusArticle.EN_VENTE); // Fetch all articles

        return articles.stream()
                .filter(article -> category == null || article.getCategory() == category)
                .filter(article -> minPrice == null || article.getPrice() >= minPrice)
                .filter(article -> maxPrice == null || article.getPrice() <= maxPrice)
                .filter(article -> name == null || article.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }
    public void create(ArticleDTO articleDTO, User user){
        Article article = new Article();
        article.setName(articleDTO.getName());
        article.setDescription(articleDTO.getDescription());
        article.setPrice(articleDTO.getPrice());
        article.setCategory(articleDTO.getCategory());
        article.setStatus(StatusArticle.EN_VENTE);
        article.setUser(user);
        articleRepository.save(article);
    }

    public boolean delete(int articleId) {
        Optional<Article> articleOptional = articleRepository.findById(articleId);
        if (articleOptional.isPresent()) {
            articleRepository.deleteById(articleId);
            return true;
        }
        return false;
    }

    public boolean update(int articleId, ArticleDTO articleDTO) {
        Optional<Article> articleOptional = articleRepository.findById(articleId);
        if (articleOptional.isPresent()) {
            Article article = articleOptional.get();
            article.setName(articleDTO.getName());
            article.setDescription(articleDTO.getDescription());
            article.setPrice(articleDTO.getPrice());
            article.setCategory(articleDTO.getCategory());
            articleRepository.save(article);
            return true;
        }
        return false;
    }

    public boolean markAsSold(int articleId) {
        Optional<Article> articleOptional = articleRepository.findById(articleId);
        if (articleOptional.isPresent()) {
            Article article = articleOptional.get();
            article.setStatus(StatusArticle.VENDU);
            articleRepository.save(article);
            return true;
        }
        return false;
    }
}
