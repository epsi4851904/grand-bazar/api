package com.example.api.controllers;

import com.example.api.entities.Article;
import com.example.api.entities.User;
import com.example.api.enums.Category;
import com.example.api.enums.StatusArticle;
import com.example.api.models.ArticleDTO;
import com.example.api.repositories.ArticleRepository;
import com.example.api.repositories.UserRepository;
import com.example.api.services.ArticleService;
import com.example.api.services.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    JwtService jwtService;

    @Autowired
    ArticleService articleService;

    @GetMapping("")
    public ResponseEntity<List<Article>> getArticles(
            @RequestParam(required = false) Category category,
            @RequestParam(required = false) Double minPrice,
            @RequestParam(required = false) Double maxPrice,
            @RequestParam(required = false) String name) {

        List<Article> articles = articleService.getArticlesByFilter(category, minPrice, maxPrice, name);
        return ResponseEntity.ok(articles);
    }

    @PostMapping("/new")
    public ResponseEntity<String> create(@RequestHeader("Authorization") String authorizationHeader, @RequestBody ArticleDTO articleDTO) {
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);

            String email = jwtService.extractEmail(token);

            Optional<User> userOptional = userRepository.findByEmail(email);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                articleService.create(articleDTO, user);
                return ResponseEntity.ok("Article créé avec succés.");
            } else {
                return ResponseEntity.status(403).body("Vous n'êtes pas autorisé");
            }
        } else {
            return ResponseEntity.status(400).body("Vous n'êtes pas autorisé.");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@RequestHeader("Authorization") String authorizationHeader, @PathVariable int id) {
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);

            String email = jwtService.extractEmail(token);

            Optional<User> userOptional = userRepository.findByEmail(email);
            if (userOptional.isPresent()) {
                boolean isDeleted = articleService.delete(id);
                if (isDeleted) {
                    return ResponseEntity.ok("Article supprimé avec succés.");
                } else {
                    return ResponseEntity.status(404).body("Article inconnu.");
                }
            } else {
                return ResponseEntity.status(403).body("Vous n'êtes pas autorisé.");
            }
        } else {
            return ResponseEntity.status(400).body("Vous n'êtes pas autorisé.");
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> update(@RequestHeader("Authorization") String authorizationHeader, @PathVariable int id, @RequestBody ArticleDTO articleDTO) {
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);

            String email = jwtService.extractEmail(token);

            Optional<User> userOptional = userRepository.findByEmail(email);
            if (userOptional.isPresent()) {
                boolean isUpdated = articleService.update(id, articleDTO);
                if (isUpdated) {
                    return ResponseEntity.ok("Article mis à jour avec succés.");
                } else {
                    return ResponseEntity.status(404).body("Article inconnu.");
                }
            } else {
                return ResponseEntity.status(403).body("Vous n'êtes pas autorisé.");
            }
        } else {
            return ResponseEntity.status(400).body("Vous nêtes pas autorisé.");
        }
    }

    @PutMapping("/vendu/{id}")
    public ResponseEntity<String> markAsSold(@RequestHeader("Authorization") String authorizationHeader, @PathVariable int id) {
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);

            String email = jwtService.extractEmail(token);

            Optional<User> userOptional = userRepository.findByEmail(email);
            if (userOptional.isPresent()) {
                boolean isMarkedAsSold = articleService.markAsSold(id);
                if (isMarkedAsSold) {
                    return ResponseEntity.ok("Article vendu avec succés.");
                } else {
                    return ResponseEntity.status(404).body("Article inconnu.");
                }
            } else {
                return ResponseEntity.status(403).body("Vous n'êtes pas autorisé.");
            }
        } else {
            return ResponseEntity.status(400).body("Vous nêtes pas autorisé.");
        }
    }
}
