package com.example.api.controllers;


import com.example.api.entities.Note;
import com.example.api.entities.User;
import com.example.api.models.NoteDTO;
import com.example.api.repositories.UserRepository;
import com.example.api.services.NoteService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/note")
public class NoteController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    NoteService noteService;

    @PostMapping("/{id}")
    public ResponseEntity<String> create(
            @RequestBody NoteDTO noteDTO,
            @PathVariable int id ){

        if(noteDTO.getNote() < 0 || noteDTO.getNote() > 5 ){
            return ResponseEntity.status(409).body("La note doit etre comprise entre 0 et 5");
        }

        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            noteService.create(noteDTO, user);
            return ResponseEntity.ok("Note créé avec succés");
        }
        else {
            return ResponseEntity.status(404).body("Aucun utilisateur trouvé");
        }

    }

}
