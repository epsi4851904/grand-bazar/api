package com.example.api.controllers;

import com.example.api.models.AuthRequest;
import com.example.api.models.AuthResponse;
import com.example.api.services.JwtService;
import com.example.api.services.UserService;
import com.example.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtService jwtService;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody User user) throws NoSuchAlgorithmException {
        userService.createUser(
                user.getEmail(),
                user.getPassword(),
                user.getFirstname(),
                user.getLastname()
        );
        return ResponseEntity.ok("User registered successfully");
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthRequest authRequest) {
        if (userService.validateUser(authRequest.getEmail(), authRequest.getPassword())) {
            String token = jwtService.generateToken(authRequest.getEmail());
            return ResponseEntity.ok(new AuthResponse(token));
        } else {
            return ResponseEntity.status(403).body("Invalid email or password");
        }
    }
}
