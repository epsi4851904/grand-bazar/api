package com.example.api.enums;

public enum Category {
    ELECTRONICS,
    CLOTHING,
    HOME_APPLIANCES,
    BOOKS,
    SPORTS_AND_OUTDOORS,
    BEAUTY,
    TOYS_AND_GAMES,
    FOOD_AND_GROCERY,
    FURNITURE,
    AUTOMOTIVE,
    OTHER;
}
