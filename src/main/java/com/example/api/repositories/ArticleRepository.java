package com.example.api.repositories;

import com.example.api.entities.Article;
import com.example.api.entities.User;
import com.example.api.enums.Category;
import com.example.api.enums.StatusArticle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends CrudRepository<Article, Integer> {
    List<Article> findByStatus(StatusArticle status);
    List<Article> findByCategory(Category category);
    @Query("SELECT a FROM Article a WHERE a.price >= :minPrice AND a.price <= :maxPrice")
    List<Article> findByPriceRange(@Param("minPrice") Double minPrice, @Param("maxPrice") Double maxPrice);

    @Query("SELECT a FROM Article a WHERE LOWER(a.name) LIKE LOWER(CONCAT('%', :name, '%'))")
    List<Article> findByNameContaining(@Param("name") String name);

}
