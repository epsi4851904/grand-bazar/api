# API SPRING - GRAND BAZAR

### Groupe 2 :
- SERVARY Nicolas
- REYNAUD Lucas
- BONNEVAL Mattéo

## Utilisation API :
 ## Lancement API:
Avant de lancer l'api il faut créer l'image MySQL : 
```shell
docker compose up -d --build
```
retrouver un phpmyadmin à ce lien : http://localhost:8080

user : root
mdp : root

### Info utiles :

Pour les routes de création, update et delete d'Article il est nécessaire d'être connecté avec un compte grâce à la route /login

Les routes pour récupérer la liste des articles, et noter les vendeurs sont public

### Filtres :

Au niveau des filtres articles nous avons mis en place :
- sur le nom => pour rechercher un produit en particulier en fonction de son nom
- sur la ctaégorie => pour rechercher en fonction d'une énumération de catégorie :
    - ELECTRONICS,
    -  CLOTHING,
    -  HOME_APPLIANCES,
    -  BOOKS,
    -  SPORTS_AND_OUTDOORS,
    -  BEAUTY,
    -  TOYS_AND_GAMES,
    -  FOOD_AND_GROCERY,
    -  FURNITURE,
    -  AUTOMOTIVE,
    -  OTHER;
- sur le prix avec une date range => pour rechercher un article entre deux prix